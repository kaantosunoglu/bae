// https://github.com/aframevr/aframe/issues/1041

module.exports = {
  entry: ["babel-polyfill", "./js/index.js"],
  output: {
    path: __dirname,
    filename: "../assets/js/bundle.js"
  },
  module: {
    noParse: [
      /node_modules\/aframe\/dist\/aframe.js/, // for aframe from NPM
      /node_modules\/cannon\/build\/cannon.js/ // for aframe-extras from NPM
    ],
    loaders: [
      {
        test: /\.js$/,
        exclude: /node_modules/,
        loader: "babel-loader",
        query: {
          presets: ["es2015"],
          plugins: ["transform-async-to-generator"]
        }
      }
    ]
  }
};
