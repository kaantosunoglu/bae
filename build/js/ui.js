// const $ = require("jquery");
// window.jQuery = $;
// window.$ = $;

// $(document).ready(function() {});

// const AFRAME = require("aframe");
// AFRAME.register([require("aframe-extras"), require("aframe-ar")]);

// require("aframe");
// require("aframe-ar");
// require("aframe-extras");

var isPlaying = false;
AFRAME.registerComponent("soundhandler", {
  tick: function() {
    if (document.querySelector("a-marker").object3D.visible == true) {
      document.getElementById("introHeader").style.display = "none";
      document.getElementById("markerDiv").style.display = "none";
      document.getElementById("logo").style.display = "none";
      document.getElementById("planeHeader").style.display = "block";
      // document.getElementById("closeButton").style.display = "block";
      document.getElementsByClassName("a-canvas")[0].style.backgroundImage =
        "url(../img/CLEAN_FIAS.png)";

      changeTexts(true);
      if (!isPlaying) {
        entity.components.sound.stopSound();
        entity.components.sound.playSound();
        isPlaying = true;
      }
    } else {
      isPlaying = false;
      document.getElementById("introHeader").style.display = "block";
      document.getElementById("markerDiv").style.display = "block";
      document.getElementById("logo").style.display = "block";
      document.getElementById("planeHeader").style.display = "none";
      // document.getElementById("closeButton").style.display = "none";
      document.getElementsByClassName("a-canvas")[0].style.backgroundImage =
        "url(../img/CLEAN_FIAS_trans.png)";
      changeTexts(false);
    }
  }
});

//Manage orientation
window.onresize = function(event) {
  applyOrientation();
};

function applyOrientation() {
  if (window.innerHeight < window.innerWidth) {
    document.getElementById("landscape").style.display = "block";
  } else {
    document.getElementById("landscape").style.display = "none";
  }
}

var entity = document.querySelector("[sound]"); //need to register this here so that the page component is already loaded

// All planes properties and informations
var planes = [
  {
    name: "bristol",
    query: "#bristol",
    goAnim: "myGLTFGo",
    comeInAnim: "myGLTFCome",
    header: "BRISTOL",
    date: "9999",
    infoText:
      "<p>BRISTOL Libero est, congue a ipsum tincidunt, suscipit sagittis nibh. Phasellus quis mi et lacus varius pellentesque sed fringilla ipsum. Donec efficitur sed ipsum at scelerisque.</p> <p> Sed sit amet turpis eget tellus tempus molestie.</p> <p> Sed sit amet turpis eget tellus tempus molestie.</p> <p> Sed sit amet turpis eget tellus tempus molestie.</p> <p> Sed sit amet turpis eget tellus tempus molestie.</p> <p> Sed sit amet turpis eget tellus tempus molestie.</p> <p> Sed sit amet turpis eget tellus tempus molestie.</p> <p> Sed sit amet turpis eget tellus tempus molestie.</p>"
  },
  {
    name: "spitfire",
    query: "#spitfire",
    goAnim: "myGLTFGo",
    comeInAnim: "myGLTFCome",
    header: "SPITFIRE",
    date: "9999",
    infoText:
      "<p>SPITFIRE Libero est, congue a ipsum tincidunt, suscipit sagittis nibh. Phasellus quis mi et lacus varius pellentesque sed fringilla ipsum. Donec efficitur sed ipsum at scelerisque.</p> <p> Sed sit amet turpis eget tellus tempus molestie.</p>"
  },
  {
    name: "mosquito",
    query: "#mosquito",
    goAnim: "myGLTFGo",
    comeInAnim: "myGLTFCome",
    header: "MOSQUITO",
    date: "9999",
    infoText:
      "<p>MOSQUITO Libero est, congue a ipsum tincidunt, suscipit sagittis nibh. Phasellus quis mi et lacus varius pellentesque sed fringilla ipsum. Donec efficitur sed ipsum at scelerisque.</p> <p> Sed sit amet turpis eget tellus tempus molestie.</p>"
  },
  {
    name: "lancaster",
    query: "#lancaster",
    goAnim: "myGLTFGo",
    comeInAnim: "myGLTFCome",
    header: "LANCASTER",
    date: "9999",
    infoText:
      "<p>LANCASTER Libero est, congue a ipsum tincidunt, suscipit sagittis nibh. Phasellus quis mi et lacus varius pellentesque sed fringilla ipsum. Donec efficitur sed ipsum at scelerisque.</p> <p> Sed sit amet turpis eget tellus tempus molestie.</p>"
  },
  {
    name: "canberra",
    query: "#canberra",
    goAnim: "myGLTFGo",
    comeInAnim: "myGLTFCome",
    header: "CANBERRA",
    date: "9999",
    infoText:
      "<p>CANBERRA Libero est, congue a ipsum tincidunt, suscipit sagittis nibh. Phasellus quis mi et lacus varius pellentesque sed fringilla ipsum. Donec efficitur sed ipsum at scelerisque.</p> <p> Sed sit amet turpis eget tellus tempus molestie.</p>"
  },
  {
    name: "harrier",
    query: "#harrier",
    goAnim: "myGLTFGo",
    comeInAnim: "myGLTFCome",
    header: "HARRIER",
    date: "9999",
    infoText:
      "<p>HARRIER Libero est, congue a ipsum tincidunt, suscipit sagittis nibh. Phasellus quis mi et lacus varius pellentesque sed fringilla ipsum. Donec efficitur sed ipsum at scelerisque.</p> <p> Sed sit amet turpis eget tellus tempus molestie.</p>"
  },
  {
    name: "hawk",
    query: "#hawk",
    goAnim: "myGLTFGo",
    comeInAnim: "myGLTFCome",
    header: "HAWK",
    date: "9999",
    infoText:
      "<p>HAWK Libero est, congue a ipsum tincidunt, suscipit sagittis nibh. Phasellus quis mi et lacus varius pellentesque sed fringilla ipsum. Donec efficitur sed ipsum at scelerisque.</p> <p> Sed sit amet turpis eget tellus tempus molestie.</p>"
  },
  {
    name: "tornado",
    query: "#tornado",
    goAnim: "myGLTFGo",
    comeInAnim: "myGLTFCome",
    header: "TORNADO",
    date: "9999",
    infoText:
      "<p>TORNADO Libero est, congue a ipsum tincidunt, suscipit sagittis nibh. Phasellus quis mi et lacus varius pellentesque sed fringilla ipsum. Donec efficitur sed ipsum at scelerisque.</p> <p> Sed sit amet turpis eget tellus tempus molestie.</p>"
  },
  {
    name: "typhoon",
    query: "#typhoon",
    goAnim: "myGLTFGo",
    comeInAnim: "myGLTFCome",
    header: "TYPHOON",
    date: "9999",
    infoText:
      "<p>TYPHOON Libero est, congue a ipsum tincidunt, suscipit sagittis nibh. Phasellus quis mi et lacus varius pellentesque sed fringilla ipsum. Donec efficitur sed ipsum at scelerisque.</p> <p> Sed sit amet turpis eget tellus tempus molestie.</p>"
  }
];
var planeNumber = 0;
var isTextChangedToIntro = false;
var isTextChangedToPlane = false;
document.getElementById("planeHeaderText").innerHTML = "";
document.getElementById("planeHeaderDate").innerHTML = "";
document.getElementById("infoText").innerHTML = "";
document.getElementById("markerDiv").style.display = "none";

function nextModel() {
  var nextPlaneNumber = planeNumber == planes.length - 1 ? 0 : planeNumber + 1;
  myMove(planeNumber, nextPlaneNumber);
  planeNumber = nextPlaneNumber;
}

function myMove(currentElement, nextElement) {
  var currentModel = document.querySelector(planes[currentElement].query);
  currentModel.emit(planes[currentElement].goAnim);

  // var nextModel = document.querySelector(planes[nextElement].query);
  // nextModel.object3D.visible = true;
}

function myMoveIn(element) {
  var prevElement = planeNumber === 0 ? planes.length - 1 : planeNumber - 1;
  var prevModel = document.querySelector(planes[prevElement].query);
  prevModel.object3D.position.x = 5;
  prevModel.object3D.position.y = 0;
  // prevModel.object3D.rotation.x = 0;
  // prevModel.object3D.rotation.y = -180;
  // prevModel.object3D.rotation.z = 90;

  prevModel.object3D.visible = false;

  var currentModel = document.querySelector(planes[element].query);
  currentModel.emit(planes[element].comeInAnim);
  currentModel.object3D.visible = true;
}

// var spitfireGo = document.querySelector('#spitfireGo');
// spitfireGo.addEventListener('animationend', function () {
//     myMoveIn(planeNumber);
// });
// var spitfireComeIn = document.querySelector('#spitfireComeIn');
// spitfireComeIn.addEventListener('animationend', function () {
//     document.getElementById("headerText").innerHTML = planes[planeNumber].header;
//     document.getElementById("infoText").innerHTML = planes[planeNumber].infoText;
// });

var bristolGo = document.querySelector("#bristolGo");
bristolGo.addEventListener("animationend", function() {
  myMoveIn(planeNumber);
});
var bristolComeIn = document.querySelector("#bristolComeIn");
bristolComeIn.addEventListener("animationend", function() {
  setTexts();
});

function changeTexts(isMarker) {
  if (isMarker == true) {
    if (isTextChangedToPlane == false) {
      isTextChangedToPlane = true;
      isTextChangedToIntro = false;
      setTexts();
      //  document.getElementById("infoTextContainer").style.width = "73%";
      document.getElementById("nextButton").style.display = "block";
    }
  } else {
    if (isTextChangedToIntro == false) {
      isTextChangedToPlane = false;
      isTextChangedToIntro = true;
      document.getElementById("headerText").innerHTML = "Above and beyond";
      document.getElementById("infoText").innerHTML =
        "<p>INTRODUCTION ii Libero est, congue a ipsum tincidunt, suscipit sagittis nibh. Phasellus quis mi et lacus varius pellentesque sed fringilla ipsum. Donec efficitur sed ipsum at scelerisque.</p><p> Sed sit amet turpis eget tellus tempus molestie.</p><p> Sed sit amet turpis eget tellus tempus molestie.</p><p> Sed sit amet turpis eget tellus tempus molestie.</p><p> Sed sit amet turpis eget tellus tempus molestie.</p><p> Sed sit amet turpis eget tellus tempus molestie.</p>";
      //  document.getElementById("infoTextContainer").style.width = "100%";
      document.getElementById("nextButton").style.display = "none";
    }
  }
}

function setTexts() {
  document.getElementById("planeHeaderText").innerHTML =
    planes[planeNumber].header;
  document.getElementById("planeHeaderDate").innerHTML =
    planes[planeNumber].date;
  document.getElementById("infoText").innerHTML = planes[planeNumber].infoText;
}

var aAssets = document.querySelector("#aasets");
var elementIndex = 1;
aAssets.addEventListener("loaded", function() {
  if (elementIndex == planes.length) {
    return;
  }

  //
  aAssets.insertAdjacentHTML(
    "beforeend",
    '<a-asset-item id="spitfire-gltf" src="OBJ/Spitfire_v1/Spitfire_Anim_v1.gltf"></a-asset-item>\
        <a-asset-item id="myMosquito-gltf" src="OBJ/Mosquito_v1/Mosquito_Anim_v2.gltf"></a-asset-item>\
        <a-asset-item id="myLancaster-gltf" src="OBJ/Lancaster_v1/Lancaster_Anim_v1.gltf"></a-asset-item>\
        <a-asset-item id="myCanberra-gltf" src="OBJ/Canberra_v1/untitled.gltf"></a-asset-item>\
        <a-asset-item id="myHarrier-gltf" src="OBJ/Harrier_v1/Harrier_v1.gltf"></a-asset-item>\
        <a-asset-item id="myHawk-gltf" src="OBJ/Hawk_v1/Hawk_v1.gltf"></a-asset-item>\
        <a-asset-item id="myTornado-gltf" src="OBJ/Tornado_v1/untitled.gltf"></a-asset-item>\
        <a-asset-item id="myTyphoon-gltf" src="OBJ/Typhoon_v2/Typhoon_v6.gltf"></a-asset-item>\
       '
  );

  var myScene = document.getElementById("myScene");
  myScene.insertAdjacentHTML(
    "beforeend",
    '\
            <a-entity animation-mixer id="lancaster" gltf-model="#myLancaster-gltf" visible="false" position="5 1 -5" rotation="0 -90 90" scale="2 2 2">\
                 <a-animation id="lancasterComeIn" attribute="position" from="5 1 -5" to="0 1 0" dur="1500" easing="ease-out" repeat="0" begin="myGLTFCome"></a-animation>\
                 <a-animation id="lancasterGo" attribute="position" from="0 1 0" to="-7 1 0" dur="2000" easing="ease-in" repeat="0" begin="myGLTFGo"></a-animation>\
                 <a-animation id="sfRotate" attribute="rotation" to="0 -90 45" dur="2000" easing="ease-in" repeat="0" begin="myGLTFGo"></a-animation>\
            </a-entity>\
            <a-entity animation-mixer id="mosquito" gltf-model="#myMosquito-gltf" visible="false" position="5 1 0" rotation="0 -90 90" scale="2 2 2">\
                <a-animation id="mosquitoComeIn" attribute="position" from="5 1 2" to="0 1 0" dur="1000" easing="ease-out" repeat="0" begin="myGLTFCome"></a-animation>\
                <a-animation id="mosquitoGo" attribute="position" from="0 1 0" to="-5 1 0" dur="2000" easing="ease-in" repeat="0" begin="myGLTFGo"></a-animation>\
                <a-animation id="sfRotate" attribute="rotation" to="0 -90 120" dur="2000" easing="ease-in" repeat="0" begin="myGLTFGo"></a-animation>\
            </a-entity>\
            <a-entity animation-mixer id="spitfire" gltf-model="#spitfire-gltf" visible="false" position="5 1 0" rotation="0 -90 90" scale="2 2 2">\
                  <a-animation id="spitfireComeIn" attribute="position" from="5 1 0" to="0 1 0" dur="1000" easing="ease-out" repeat="0" begin="myGLTFCome"></a-animation>\
                  <a-animation id="spitfireComeInRotation" attribute="rotation" from="0 -90 0" to="0 -90 90" dur="1500" easing="ease-in" repeat="0" begin="myGLTFCome"></a-animation>\
                  <a-animation id="spitfireGo" attribute="position" from="0 1 0" to="-8 -3 1" dur="2000" easing="ease-in" repeat="0" begin="myGLTFGo"></a-animation>\
                  <a-animation id="sfRotate" attribute="rotation" to="0 -90 180" dur="2200" easing="ease-out-cubic" repeat="0" begin="myGLTFGo"></a-animation>\
            </a-entity>\
            <a-entity animation-mixer id="canberra" gltf-model="#myCanberra-gltf" visible="false" position="5 1 0" rotation="0 -90 90" scale="2 2 2">\
                  <a-animation id="canberraComeIn" attribute="position" from="5 1 0" to="0 1 0" dur="1000" easing="ease-out" repeat="0" begin="myGLTFCome"></a-animation>\
                  <a-animation id="canberraGo" attribute="position" from="0 1 0" to="-7 1 3" dur="2000" easing="ease-in" repeat="0" begin="myGLTFGo"></a-animation>\
                  <a-animation id="sfRotate" attribute="rotation" to="0 -45 90" dur="6000" easing="ease-in" repeat="0" begin="myGLTFGo"></a-animation>\
            </a-entity>\
            <a-entity animation-mixer id="harrier" gltf-model="#myHarrier-gltf" visible="false" position="5 1 0" rotation="0 -90 90" scale="2 2 2">\
                <a-animation id="harrierComeIn" attribute="position" from="5 1 -2" to="0 1 0" dur="900" easing="ease-out" repeat="0" begin="myGLTFCome"></a-animation>\
                <a-animation id="harrierComeInRotation" attribute="rotation" from="0 -70 45" to="0 -90 90" dur="1500" easing="ease-in" repeat="0" begin="myGLTFCome"></a-animation>\
                <a-animation id="harrierGo" attribute="position" from="0 1 0" to="-7 1 0" dur="1000" easing="ease-in" repeat="0" begin="myGLTFGo"></a-animation>\
            </a-entity>\
            <a-entity animation-mixer id="hawk" gltf-model="#myHawk-gltf" visible="false" position="5 1 0" rotation="0 -90 90" scale="2 2 2">\
                <a-animation id="hawkComeIn" attribute="position" from="5 1 0" to="0 1 0" dur="1000" easing="ease-out" repeat="0" begin="myGLTFCome"></a-animation>\
                <a-animation id="harrierComeInRotation" attribute="rotation" from="30 -90 0" to="0 -90 90" dur="1500" easing="ease-in" repeat="0" begin="myGLTFCome"></a-animation>\
                <a-animation id="hawkGo" attribute="position" from="0 1 0" to="-6 1 0" dur="1200" easing="ease-in" repeat="0" begin="myGLTFGo"></a-animation>\
                <a-animation id="sfRotate" attribute="rotation" to="0 -90 135" dur="400" easing="ease-in" repeat="0" begin="myGLTFGo"></a-animation>\
            </a-entity>\
            <a-entity animation-mixer id="tornado" gltf-model="#myTornado-gltf" visible="false" position="5 1 0" rotation="0 -90 90" scale="2 2 2">\
                <a-animation id="tornadoComeIn" attribute="position" from="5 1 2" to="0 1 0" dur="1000" easing="ease-out" repeat="0" begin="myGLTFCome"></a-animation>\
                <a-animation id="tornadoGo" attribute="position" from="0 1 0" to="-5 1 -2" dur="1000" easing="ease-in" repeat="0" begin="myGLTFGo"></a-animation>\
                <a-animation id="sfRotate" attribute="rotation" to="0 -125 90" dur="2000" easing="ease-in" repeat="0" begin="myGLTFGo"></a-animation>\
            </a-entity>\
            <a-entity animation-mixer id="typhoon" gltf-model="#myTyphoon-gltf" visible="false" position="5 1 0" rotation="0 -90 90" scale="2 2 2">\
                <a-animation id="typhoonComeIn" attribute="position" from="5 1 0" to="0 1 0" dur="1000" easing="ease-out" repeat="0" begin="myGLTFCome"></a-animation>\
                <a-animation id="typhoonGo" attribute="position" from="0 1 0" to="-5 1 0" dur="1000" easing="ease-in" repeat="0" begin="myGLTFGo"></a-animation>\
                <a-animation id="sfRotate" attribute="rotation" to="0 -90 -90" dur="1000" easing="ease-in" repeat="0" begin="myGLTFGo"></a-animation>\
            </a-entity>'
  );

  var spitfireGo = document.querySelector("#spitfireGo");
  spitfireGo.addEventListener("animationend", function() {
    myMoveIn(planeNumber);
  });
  var spitfireComeIn = document.querySelector("#spitfireComeIn");
  spitfireComeIn.addEventListener("animationend", function() {
    setTexts();
  });

  var mosquitoGo = document.querySelector("#mosquitoGo");
  mosquitoGo.addEventListener("animationend", function() {
    myMoveIn(planeNumber);
  });
  var mosquitoComeIn = document.querySelector("#mosquitoComeIn");
  mosquitoComeIn.addEventListener("animationend", function() {
    setTexts();
  });

  var lancestarGo = document.querySelector("#lancasterGo");
  lancestarGo.addEventListener("animationend", function() {
    myMoveIn(planeNumber);
  });
  var lancasterComeIn = document.querySelector("#lancasterComeIn");
  lancasterComeIn.addEventListener("animationend", function() {
    setTexts();
  });

  var canberraGo = document.querySelector("#canberraGo");
  canberraGo.addEventListener("animationend", function() {
    myMoveIn(planeNumber);
  });
  var canberraComeIn = document.querySelector("#canberraComeIn");
  canberraComeIn.addEventListener("animationend", function() {
    setTexts();
  });

  var harrierGo = document.querySelector("#harrierGo");
  harrierGo.addEventListener("animationend", function() {
    myMoveIn(planeNumber);
  });
  var harrierComeIn = document.querySelector("#harrierComeIn");
  harrierComeIn.addEventListener("animationend", function() {
    setTexts();
  });

  var hawkGo = document.querySelector("#hawkGo");
  hawkGo.addEventListener("animationend", function() {
    myMoveIn(planeNumber);
  });
  var hawkComeIn = document.querySelector("#hawkComeIn");
  hawkComeIn.addEventListener("animationend", function() {
    setTexts();
  });

  var tornadoGo = document.querySelector("#tornadoGo");
  tornadoGo.addEventListener("animationend", function() {
    myMoveIn(planeNumber);
  });
  var tornadoComeIn = document.querySelector("#tornadoComeIn");
  tornadoComeIn.addEventListener("animationend", function() {
    setTexts();
  });

  var typhoonGo = document.querySelector("#typhoonGo");
  typhoonGo.addEventListener("animationend", function() {
    myMoveIn(planeNumber);
  });
  var typhoonComeIn = document.querySelector("#typhoonComeIn");
  typhoonComeIn.addEventListener("animationend", function() {
    setTexts();
  });
});
