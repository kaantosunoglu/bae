"use strict";

const gulp = require("gulp");
const fs = require("fs");

// Sass
const sass = require("gulp-sass");
const sourcemaps = require("gulp-sourcemaps");
const autoprefixer = require("gulp-autoprefixer");
const cleanCSS = require("gulp-clean-css");
const moduleImporter = require("sass-module-importer");
const htmlmin = require("gulp-htmlmin");

const autoprefixerOptions = {
  browsers: ["last 2 versions", "> 5%", "Firefox ESR"]
};

const config = {
  sassInputDir: "./sass/style.scss",
  sassOutputDir: "../assets/css",
  nodeDir: "../build/node_modules",
  assetsDir: "../assets"
};

// JS Webpack
var gutil = require("gulp-util");
var webpack = require("webpack");
var webpackConfig = require("./webpack.config.js");

// JS Development
var myDevConfig = Object.create(webpackConfig);
myDevConfig.devtool = "sourcemap";
myDevConfig.debug = false;
var devCompiler = webpack(myDevConfig);

gulp.task("webpack:build-dev", function(callback) {
  devCompiler.run(function(err, stats) {
    if (err) throw new gutil.PluginError("webpack:build-dev", err);
    gutil.log(
      "[webpack:build-dev]",
      stats.toString({
        colors: true
      })
    );
    callback();
  });
});

// JS Build
gulp.task("webpack:build", function(callback) {
  var myConfig = Object.create(webpackConfig);
  myConfig.plugins = [
    new webpack.DefinePlugin({
      "process.env": {
        NODE_ENV: JSON.stringify("production")
      }
    }),
    new webpack.optimize.DedupePlugin(),
    new webpack.optimize.UglifyJsPlugin({
      compress: {
        warnings: false
      },
      output: {
        comments: false
      }
    })
  ];

  webpack(myConfig, function(err, stats) {
    if (err) throw new gutil.PluginError("webpack:build", err);
    gutil.log(
      "[webpack:build]",
      stats.toString({
        colors: true
      })
    );
    callback();
  });
});

// Copy index.htm and paste index.html
gulp.task("duplicatehtml", function() {
  return fs
    .createReadStream("../index.htm")
    .pipe(fs.createWriteStream("../index.html"));
});

// HTML Min
gulp.task("htmlmin", function() {
  return gulp
    .src("../index.html")
    .pipe(
      htmlmin({
        collapseWhitespace: true,
        minifyJS: true,
        minifyCSS: true,
        removeComments: true
      })
    )
    .pipe(gulp.dest("../"));
});

// Imagemin
const imagemin = require("gulp-imagemin");
const pngquant = require("imagemin-pngquant");

// Sass Develop
gulp.task("sass-develop", function() {
  return gulp
    .src(config.sassInputDir)
    .pipe(sass({ importer: moduleImporter() }))
    .pipe(sourcemaps.init())
    .pipe(
      sass({ outputStyle: "expanded", errLogToConsole: true }).on(
        "error",
        sass.logError
      )
    )
    .pipe(sourcemaps.write())
    .pipe(autoprefixer(autoprefixerOptions))
    .pipe(gulp.dest(config.sassOutputDir));
});

// Sass Product
gulp.task("sass-product", function() {
  return gulp
    .src(config.sassInputDir)
    .pipe(sass({ importer: moduleImporter() }))
    .pipe(sass({ outputStyle: "compressed" }))
    .pipe(autoprefixer(autoprefixerOptions))
    .pipe(cleanCSS({ compatibility: "ie9" }))
    .pipe(gulp.dest(config.sassOutputDir));
});

// Copy Fontawesome fonts from node_modules to assets folder
gulp.task("copy-fafonts", function() {
  return gulp
    .src(config.nodeDir + "/font-awesome/fonts/**.*")
    .pipe(gulp.dest(config.assetsDir + "/fonts"));
});

// Copy Flag icon icons from node_modules to assets folder
gulp.task("copy-flagicons", function() {
  return gulp
    .src(config.nodeDir + "/flag-icon-css/flags/**/*")
    .pipe(gulp.dest(config.assetsDir + "/flags"));
});

// Copy Weather icon icons from node_modules to assets folder
gulp.task("copy-weathericons", function() {
  return gulp
    .src(config.nodeDir + "/weathericons/font/**.*")
    .pipe(gulp.dest(config.assetsDir + "/weather-icons"));
});

// Image minimization
gulp.task("imagemin", function() {
  return gulp
    .src("assets/img/*")
    .pipe(
      imagemin({
        optimizationLevel: 5, // 0 to 7max optimization
        progressive: true,
        svgoPlugins: [{ removeViewBox: false }, { cleanupIDs: false }],
        use: [pngquant()]
      })
    )
    .pipe(gulp.dest("assets/img/"));
});

gulp.task("watch", () => {
  gulp.watch("js/**/*.js", ["webpack:build-dev"]);
  gulp.watch("sass/**/*.scss", ["sass-develop"]);
});

// define tasks here
gulp.task("default", ["js-product", "sass-product", "imagemin"]);
gulp.task("build", [
  "duplicatehtml",
  "htmlmin",
  "webpack:build",
  "sass-product"
]);
