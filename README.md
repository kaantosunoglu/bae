# Voice AR Playground

Initial commit :: jeromeetienne committed on 17 Feb 2017

Marker Trainer:
https://jeromeetienne.github.io/AR.js/three.js/examples/marker-training/examples/generator.html

AR.js The Simplest Way to get Cross-Browser Augmented Reality on the Web.
https://medium.com/chialab-open-source/ar-js-the-simpliest-way-to-get-cross-browser-ar-on-the-web-8f670dd45462

glTF 2.0
https://www.khronos.org/gltf/

AR.js GITHUB
https://github.com/jeromeetienne/ar.js

A-Frame
https://aframe.io/blog/arjs/

Tracery
http://tracery.io
http://www.crystalcodepalace.com/traceryTut.html

## Development

For development
use sass and js files under build folder to develop. Compiled files will be created in assets folder

```
gulp watch
```

For build

```
gulp build
```
