var entity = document.querySelector("[sound]"); //need to register this here so that the page component is already loaded

// All planes properties and informations

var planes = [{
        name: "bristol",
        query: "#bristol",
        goAnim: "myGLTFGo",
        comeInAnim: "myGLTFCome",
        header: "Bristol F2B Fighter",
        date: "1917",
        infoText: "<ul class='planeInfo'><li>The Bristol Fighter was one of the most important and successful British aircraft to serve during the First World War.</li><li>In peacetime, the Bristol Fighter was used as an Army Cooperation machine, particularly in India and Iraq.</li><li>Today there are 3 remaining airworthy aircraft with a number on static display, predominantly around the UK and Europe.</li></ul>"
    },
    {
        name: "spitfire",
        query: "#spitfire",
        goAnim: "myGLTFGo",
        comeInAnim: "myGLTFCome",
        header: "Spitfire",
        date: "1938",
        infoText: "<ul class='planeInfo'><li>The Spitfire was designed as a short-range, high-performance interceptor aircraft.</li><li>The Spitfire is known for  its iconic elliptical wing.</li><li>During its operational career, the development of the Spitfire included a doubling of engine power, improving speed and maximum altitude by 30% for an aircraft that was 50% heavier.</li></ul>"
    },
    {
        name: "mosquito",
        query: "#mosquito",
        goAnim: "myGLTFGo",
        comeInAnim: "myGLTFCome",
        header: "Mosquito",
        date: "1941",
        infoText: "<ul class='planeInfo'><li>Originally conceived as a high-flying, unarmed photo-reconnaissance aircraft, the Mosquito saw service in wide-ranging roles from bomber, fighter-bomber, night-fighter, anti-shipping strike, trainer, torpedo bomber and target tug.</li><li>On entry into service the Mosquito was immediately successful and became well-known for its bombing, pathfinder and precision, low-level strike capabilities.</li><li>The Mosquito flew  its last war mission on 21st May 1945 when it joined the hunt for German submarines that might have been tempted to disobey the surrender order.</li></ul>"
    },
    {
        name: "lancaster",
        query: "#lancaster",
        goAnim: "myGLTFGo",
        comeInAnim: "myGLTFCome",
        header: "Lancaster",
        date: "1942",
        infoText: "<ul class='planeInfo'><li>The Lancaster had a long, unobstructed bomb bay, which meant it could carry the largest bombs used by the RAF.</li><li>Although the Lancaster was primarily a night bomber, it excelled in many other roles including daylight and precision bombing.</li><li>Postwar, the Lancaster took on the role of long range anti-submarine patrol aircraft and air-sea rescue.</li></ul>"
    },
    {
        name: "canberra",
        query: "#canberra",
        goAnim: "myGLTFGo",
        comeInAnim: "myGLTFCome",
        header: "Canberra",
        date: "1951",
        infoText: "<ul class='planeInfo'><li>The Canberra set a number of records including the first jet-powered, non-stop transatlantic flight in 1951.</li><li>It could fly at a higher altitude than any other bomber throughout the 1950s and set a world altitude record of 70,310ft in 1957.</li><li>The aircraft received the name Canberra in 1950 to honour the Australian capital – the Australian government had committed to purchasing the aircraft while it was still in development.</li></ul>"
    },
    {
        name: "harrier",
        query: "#harrier",
        goAnim: "myGLTFGo",
        comeInAnim: "myGLTFCome",
        header: "Harrier GR1",
        date: "1969",
        infoText: "<ul class='planeInfo'><li>Informally referred to as the Jump Jet it pioneered operational vertical take off and landing for jet aircraft, although its vectored thrust was more often used to produce short take-offs.</li><li>In May 1969 Harrier’s capabilities were demonstrated when an aircraft flew between St Pancras Railway Station and downtown Manhattan in the US with the use of aerial refuelling – the journey was completed in 6 hours and 11 minutes.</li><li>Harrier’s most high profile and important success was during the 1982 Falklands conflict when it was used as a ground attack aircraft, and the Sea Harrier was the only fixed-wing fighter available to protect the British Task Force.</li></ul>"
    },
    {
        name: "hawk",
        query: "#hawk",
        goAnim: "myGLTFGo",
        comeInAnim: "myGLTFCome",
        header: "Hawk",
        date: "1976",
        infoText: "<ul class='planeInfo'><li>The Hawk is the world’s most successful and proven military aircraft trainer and has been used to train more than 20,000 pilots in air forces across the world.</li><li>It provides students with a unique ‘brain training’ environment to prepare them for life in the cockpit of advanced combat jets like the Typhoon and F-35.</li><li>The Hawk is also a proven light combat aircraft, able to offer close support, reconnaissance, surveillance and air defence.</li></ul>"
    },
    {
        name: "tornado",
        query: "#tornado",
        goAnim: "myGLTFGo",
        comeInAnim: "myGLTFCome",
        header: "Tornado",
        date: "1979",
        infoText: "<ul class='planeInfo'><li>The Tornado is the RAF’s primary ground attack aircraft and also fulfils an important reconnaissance role.</li><li>It is renowned for its ability to operate in any weather conditions, at low level at any time of the day or night.</li><li>It is currently in active service for the RAF in Iraq and Syria.</li></ul>"
    },
    {
        name: "typhoon",
        query: "#typhoon",
        goAnim: "myGLTFGo",
        comeInAnim: "myGLTFCome",
        header: "Eurofighter Typhoon",
        date: "2003",
        infoText: "<ul class='planeInfo'><li>Brakes-off to take-off in less than 8 seconds, supersonic in less than 30 seconds, Typhoon can fly from London to Paris in under 9 minutes.</li><li>The aircraft can pull up to 9G turns which is the equivalent of 30 elephants pushing down on the wings.</li><li>Typhoon can carry around 7 tonnes of fuel, which would allow a small car to travel round the Earth 4.6 times.</li></ul>"
    }

]

var planeNumber = 0;
var isTextChangedToIntro = false;
var isTextChangedToPlane = false;
var isAnimationPlaying = false;
document.getElementById("planeHeaderText").innerHTML = "";
document.getElementById("planeHeaderDate").innerHTML = "";
document.getElementById("infoText").innerHTML = "";
document.getElementById("markerDiv").style.display = "none";

function nextModel() {
  if (isAnimationPlaying) return;

  var nextPlaneNumber = planeNumber == planes.length - 1 ? 0 : planeNumber + 1;
  myMove(planeNumber, nextPlaneNumber);
  planeNumber = nextPlaneNumber;
  isAnimationPlaying = true;
}

function myMove(currentElement, nextElement) {
  var currentModel = document.querySelector(planes[currentElement].query);
  currentModel.emit(planes[currentElement].goAnim);
}

function myMoveIn(element) {
  var prevElement = planeNumber === 0 ? planes.length - 1 : planeNumber - 1;
  var prevModel = document.querySelector(planes[prevElement].query);
  // prevModel.object3D.position.x = 5;
  // prevModel.object3D.position.y = 0;

  prevModel.object3D.visible = false;

  var currentModel = document.querySelector(planes[element].query);
  currentModel.emit(planes[element].comeInAnim);
  currentModel.object3D.visible = true;
}

var bristolGo = document.querySelector("#bristolGo");
bristolGo.addEventListener("animationend", function() {
  myMoveIn(planeNumber);
});
var bristolComeIn = document.querySelector("#bristolComeIn");
bristolComeIn.addEventListener("animationend", function() {
  setTexts();
});

function changeTexts(isMarker) {
  if (isMarker == true) {
    if (isTextChangedToPlane == false) {
      isTextChangedToPlane = true;
      isTextChangedToIntro = false;
      setTexts();
      //  document.getElementById("infoTextContainer").style.width = "73%";
      document.getElementById("nextButton").style.display = "block";
    }
  } else {
    if (isTextChangedToIntro == false) {
      isTextChangedToPlane = false;
      isTextChangedToIntro = true;
      document.getElementById("headerText").innerHTML =
        "<span style='color:#ff3507'>Scanning ....";
      document.getElementById("infoText").innerHTML =
        "<p>Point your camera at this trigger image on the BAE Systems poster to enable Augmented Reality.</p>";
      //  document.getElementById("infoTextContainer").style.width = "100%";
      document.getElementById("nextButton").style.display = "none";
    }
  }
}

function setTexts() {
  document.getElementById("planeHeaderText").innerHTML =
    planes[planeNumber].header;
  document.getElementById("planeHeaderDate").innerHTML =
    planes[planeNumber].date;
  document.getElementById("infoText").innerHTML = planes[planeNumber].infoText;
  isAnimationPlaying = false;
}

var aAssets = document.querySelector("#aasets");
var elementIndex = 1;
aAssets.addEventListener("loaded", function() {
  if (elementIndex == planes.length) {
    return;
  }

  //
  aAssets.insertAdjacentHTML(
    "beforeend",
    '<a-asset-item id="spitfire-gltf" src="OBJ/Spitfire_v1/Spitfire_Anim_v1.gltf"></a-asset-item>\
        <a-asset-item id="myMosquito-gltf" src="OBJ/Mosquito_v1/Mosquito_Anim_v2.gltf"></a-asset-item>\
        <a-asset-item id="myLancaster-gltf" src="OBJ/Lancaster_v1/Lancaster_Anim_v1.gltf"></a-asset-item>\
        <a-asset-item id="myCanberra-gltf" src="OBJ/Canberra_v1/untitled.gltf"></a-asset-item>\
        <a-asset-item id="myHarrier-gltf" src="OBJ/Harrier_v1/Harrier_v1.gltf"></a-asset-item>\
        <a-asset-item id="myHawk-gltf" src="OBJ/Hawk_v1/Hawk_v4.gltf"></a-asset-item>\
        <a-asset-item id="myTornado-gltf" src="OBJ/Tornado_v1/untitled.gltf"></a-asset-item>\
        <a-asset-item id="myTyphoon-gltf" src="OBJ/Typhoon_v2/Typhoon_v6.gltf"></a-asset-item>\
       '
  );

  var myScene = document.getElementById("myScene");
  myScene.insertAdjacentHTML(
    "beforeend",
    '\
        <a-entity animation-mixer id="spitfire" gltf-model="#spitfire-gltf" visible="false" position="16 -10 0" rotation="0 -90 -90" scale="1.4 1.4 2.75">\
            <a-animation id="spitfireComeInRotation" attribute="rotation" from="0 -90 -90" to="0 -90 90" dur="1500" easing="linear" repeat="0" begin="myGLTFCome"></a-animation>\
            <a-animation id="spitfireComeIn" attribute="position" from="10 0 -2" to="0 0 0" dur="1750" easing="ease-out" repeat="0" begin="myGLTFCome"></a-animation>\
            <a-animation id="spitfireGo" attribute="position" from="0 0 0" to="-10 0 -2" dur="1750" easing="ease-in" repeat="0" begin="myGLTFGo"></a-animation>\
            <a-animation id="sfRotate" attribute="rotation" to="0 -90 125" dur="1500" easing="linear" repeat="0" begin="myGLTFGo"></a-animation>\
        </a-entity>\
        <a-entity animation-mixer id="mosquito" gltf-model="#myMosquito-gltf" visible="false" position="16 -10 0" rotation="-60 -90 0" scale="1.4 1.4 2.75">\
            <a-animation id="mosquitoComeInRotation" attribute="rotation" from="0 -90 0" to="0 -90 90" dur="1500" easing="linear" repeat="0" begin="myGLTFCome"></a-animation>\
            <a-animation id="mosquitoComeIn" attribute="position" from="10 0 -2" to="0 0 0" dur="1750" easing="ease-out" repeat="0" begin="myGLTFCome"></a-animation>\
            <a-animation id="mosquitoGo" attribute="position" from="0 0 0" to="-10 0 -2" dur="1750" easing="ease-in" repeat="0" begin="myGLTFGo"></a-animation>\
            <a-animation id="sfRotate" attribute="rotation" to="0 -90 45" dur="1500" easing="linear" repeat="0" begin="myGLTFGo"></a-animation>\
        </a-entity>\
            <a-entity animation-mixer id="lancaster" gltf-model="#myLancaster-gltf" visible="false" position="16 -10 0" rotation="0 -90 45" scale=".75 .75 1.5">\
            <a-animation id="lancasterComeInRotation" attribute="rotation" from="0 -90 45" to="0 -90 90" dur="1500" easing="linear" repeat="0" begin="myGLTFCome"></a-animation>\
            <a-animation id="lancasterComeIn" attribute="position" from="10 0 -6" to="0 0 0" dur="1750" easing="ease-out" repeat="0" begin="myGLTFCome"></a-animation>\
            <a-animation id="lancasterGo" attribute="position" from="0 0 0" to="-10 0 -2" dur="1750" easing="ease-in" repeat="0" begin="myGLTFGo"></a-animation>\
            <a-animation id="sfRotate" attribute="rotation" to="0 -90 45" dur="1500" easing="linear" repeat="0" begin="myGLTFGo"></a-animation>\
        </a-entity>\
        <a-entity animation-mixer id="canberra" gltf-model="#myCanberra-gltf" visible="false" position="16 -10 0" rotation="0 -90 0" scale="1 1 1.5">\
            <a-animation id="canberraComeInRotation" attribute="rotation" from="0 -90 0" to="0 -90 90" dur="1500" easing="linear" repeat="0" begin="myGLTFCome"></a-animation>\
            <a-animation id="canberraComeIn" attribute="position" from="10 0 -2" to="0 0 0" dur="1750" easing="ease-out" repeat="0" begin="myGLTFCome"></a-animation>\
            <a-animation id="canberraGo" attribute="position" from="0 0 0" to="-10 0 -6" dur="1750" easing="ease-in" repeat="0" begin="myGLTFGo"></a-animation>\
            <a-animation id="sfRotate" attribute="rotation" to="0 -90 120" dur="1500" easing="linear" repeat="0" begin="myGLTFGo"></a-animation>\
        </a-entity>\
        <a-entity animation-mixer id="harrier" gltf-model="#myHarrier-gltf" visible="false" position="16 -10 0" rotation="-60 -70 55" scale="1.2 1.2 1.5">\
            <a-animation id="harrierComeInRotation" attribute="rotation" from="0 -90 -60" to="0 -90 90" dur="1500" easing="linear" repeat="0" begin="myGLTFCome"></a-animation>\
            <a-animation id="harrierComeIn" attribute="position" from="10 0 -2" to="0 0 0" dur="1750" easing="ease-out" repeat="0" begin="myGLTFCome"></a-animation>\
            <a-animation id="harrierGo" attribute="position" from="0 0 0" to="-10 0 -2" dur="1750" easing="ease-in" repeat="0" begin="myGLTFGo"></a-animation>\
            <a-animation id="sfRotate" attribute="rotation" to="0 -90 180" dur="500" easing="linear" repeat="0" begin="myGLTFGo"></a-animation>\
        </a-entity>\
        <a-entity animation-mixer id="hawk" gltf-model="#myHawk-gltf" visible="false" position="16 -10 0" rotation="-60 -90 0" scale="1.5 1.5 2.4">\
            <a-animation id="hawkComeInRotation" attribute="rotation" from="0 -90 120" to="0 -90 90" dur="1500" easing="linear" repeat="0" begin="myGLTFCome"></a-animation>\
            <a-animation id="hawkComeIn" attribute="position" from="10 0 -2" to="0 0 0" dur="1750" easing="ease-out" repeat="0" begin="myGLTFCome"></a-animation>\
            <a-animation id="hawkGo" attribute="position" from="0 0 0" to="-18 -3 -10" dur="1750" easing="ease-in" repeat="0" begin="myGLTFGo"></a-animation>\
            <a-animation id="sfRotate" attribute="rotation" to="0 -90 135" dur="400" easing="linear" repeat="0" begin="myGLTFGo"></a-animation>\
        </a-entity>\
        <a-entity animation-mixer id="tornado" gltf-model="#myTornado-gltf" visible="false" position="16 -10 0" rotation="-60 -90 0" scale="1.125 1.125 1.75">\
            <a-animation id="tornadoComeInRotation" attribute="rotation" from="0 -90 120" to="0 -90 90" dur="1500" easing="linear" repeat="0" begin="myGLTFCome"></a-animation>\
            <a-animation id="tornadoComeIn" attribute="position" from="10 0 -2" to="0 0 0" dur="1750" easing="ease-out" repeat="0" begin="myGLTFCome"></a-animation>\
            <a-animation id="tornadoGo" attribute="position" from="0 0 0" to="-10 0 -2" dur="1750" easing="ease-in" repeat="0" begin="myGLTFGo"></a-animation>\
            <a-animation id="sfRotate" attribute="rotation" to="0 -90 60" dur="1500" easing="linear" repeat="0" begin="myGLTFGo"></a-animation>\
        </a-entity>\
        <a-entity animation-mixer id="typhoon" gltf-model="#myTyphoon-gltf" visible="false" position="16 -10 0" rotation="-60 -90 0" scale="1.125 1.125 1.7">\
            <a-animation id="typhoonComeInRotation" attribute="rotation" from="0 -90 120" to="0 -90 90" dur="1500" easing="linear" repeat="0" begin="myGLTFCome"></a-animation>\
            <a-animation id="typhoonComeIn" attribute="position" from="10 0 -2" to="0 0 0" dur="1750" easing="ease-out" repeat="0" begin="myGLTFCome"></a-animation>\
            <a-animation id="typhoonGo" attribute="position" from="0 0 0" to="-10 0 -2" dur="1750" easing="ease-in" repeat="0" begin="myGLTFGo"></a-animation>\
            <a-animation id="sfRotate" attribute="rotation" to="0 -90 0" dur="1500" easing="linear" repeat="0" begin="myGLTFGo"></a-animation>\
        </a-entity>'
  );

  var spitfireGo = document.querySelector("#spitfireGo");
  spitfireGo.addEventListener("animationend", function() {
    myMoveIn(planeNumber);
  });
  var spitfireComeIn = document.querySelector("#spitfireComeIn");
  spitfireComeIn.addEventListener("animationend", function() {
    setTexts();
  });

  var mosquitoGo = document.querySelector("#mosquitoGo");
  mosquitoGo.addEventListener("animationend", function() {
    myMoveIn(planeNumber);
  });
  var mosquitoComeIn = document.querySelector("#mosquitoComeIn");
  mosquitoComeIn.addEventListener("animationend", function() {
    setTexts();
  });

  var lancestarGo = document.querySelector("#lancasterGo");
  lancestarGo.addEventListener("animationend", function() {
    myMoveIn(planeNumber);
  });
  var lancasterComeIn = document.querySelector("#lancasterComeIn");
  lancasterComeIn.addEventListener("animationend", function() {
    setTexts();
  });

  var canberraGo = document.querySelector("#canberraGo");
  canberraGo.addEventListener("animationend", function() {
    myMoveIn(planeNumber);
  });
  var canberraComeIn = document.querySelector("#canberraComeIn");
  canberraComeIn.addEventListener("animationend", function() {
    setTexts();
  });

  var harrierGo = document.querySelector("#harrierGo");
  harrierGo.addEventListener("animationend", function() {
    myMoveIn(planeNumber);
  });
  var harrierComeIn = document.querySelector("#harrierComeIn");
  harrierComeIn.addEventListener("animationend", function() {
    setTexts();
  });

  var hawkGo = document.querySelector("#hawkGo");
  hawkGo.addEventListener("animationend", function() {
    myMoveIn(planeNumber);
  });
  var hawkComeIn = document.querySelector("#hawkComeIn");
  hawkComeIn.addEventListener("animationend", function() {
    setTexts();
  });

  var tornadoGo = document.querySelector("#tornadoGo");
  tornadoGo.addEventListener("animationend", function() {
    myMoveIn(planeNumber);
  });
  var tornadoComeIn = document.querySelector("#tornadoComeIn");
  tornadoComeIn.addEventListener("animationend", function() {
    setTexts();
  });

  var typhoonGo = document.querySelector("#typhoonGo");
  typhoonGo.addEventListener("animationend", function() {
    myMoveIn(planeNumber);
  });
  var typhoonComeIn = document.querySelector("#typhoonComeIn");
  typhoonComeIn.addEventListener("animationend", function() {
    setTexts();
  });
});
